package com.formation.Spring.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Notes")
public class Notes {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(nullable = false)
	private int notes_id;
	private int sport;
	private int social;
	private int performance;
	private int assiduite;
	private int annee;
	
	
	
	public Notes(int sport, int social, int performance, int assiduite, int annee) {
		
		this.sport = sport;
		this.social = social;
		this.performance = performance;
		this.assiduite = assiduite;
		this.annee = annee;
	}
	public int getNotes_id() {
		return notes_id;
	}
	public void setNotes_id(int notes_id) {
		this.notes_id = notes_id;
	}
	public int getSport() {
		return sport;
	}
	public void setSport(int sport) {
		this.sport = sport;
	}
	public int getSocial() {
		return social;
	}
	public void setSocial(int social) {
		this.social = social;
	}
	public int getPerformance() {
		return performance;
	}
	public void setPerformance(int performance) {
		this.performance = performance;
	}
	public int getAssiduite() {
		return assiduite;
	}
	public void setAssiduite(int assiduite) {
		this.assiduite = assiduite;
	}
	
	public int getAnnee() {
		return annee;
	}
	public void setAnnee(int annee) {
		this.annee = annee;
	}
	
	
	
}
