package com.formation.Spring.models;

import javax.persistence.*;

import org.springframework.beans.factory.annotation.Autowired;


@Entity	
@Table(name="address")
public class Address {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="address_id")
	private Integer id;

	@Column(name="address_number")
	private String number;
	
	@Column(name="address_street")
	private String street;
	
	@Column(name="address_zipCode")
	private String zipCode;
	
	@Column(name="address_city")
	private String city;
	
	@ManyToOne(cascade= {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH})
	@JoinColumn(name="address_empId", referencedColumnName="ID")
	private Employee employee;

	
	
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getNumber() {
		return number;
	}
	
	
	public Address(String number, String street, String zipCode, String city) {
	
		this.number = number;
		this.street = street;
		this.zipCode = zipCode;
		this.city = city;
	}


	public Address() {
		
	}
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}


	@Override
	public String toString() {
		return "Adress [getId()=" + getId() + ", getNumber()=" + getNumber() + ", getStreet()=" + getStreet()
				+ ", getZipCode()=" + getZipCode() + ", getCity()=" + getCity()
				+ "]";
	}
}
	
	
	
	
	
