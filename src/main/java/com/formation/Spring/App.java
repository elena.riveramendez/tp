package com.formation.Spring;

import java.util.ArrayList;
import java.util.List;

//import java.util.ArrayList;
//import java.util.List;


import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.formation.Spring.config.AppConfig;
import com.formation.Spring.config.JPAConfig;
import com.formation.Spring.models.Address;
import com.formation.Spring.models.Employee;
import com.formation.Spring.models.Notes;
import com.formation.Spring.service.AddressService;
import com.formation.Spring.service.EmployeeService;
import com.formation.Spring.service.NotesService;

public class App 
{


	public static void main( String[] args )
	{

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
		context.register(AppConfig.class, JPAConfig.class);
		context.refresh();

		Employee emp1 = new Employee("login", "password", "nom", "prenom", "mail", "role");
		Employee emp2 = new Employee("toto", "qwerty", "Sherman", "Paula", "p.sherman@ddd.com", "dentiste");
		Employee emp3 = new Employee("tutu", "ptutu", "Mariano", "Pedro", "pepe@gmail.com", "rex");

		EmployeeService empService = context.getBean(EmployeeService.class);
		empService.createOrUpdateEmployee(emp1);
		empService.createOrUpdateEmployee(emp2);
		empService.createOrUpdateEmployee(emp3);
		
		Address add1 = new Address("2", "Sesame", "89632", "NYC");
		Address add2 = new Address("24", "Walaby", "48759", "Sidney");
		Address add3 = new Address("79", "Fofo","23231", "Sao Paulo");
		Address add4 = new Address("11", "Moulinet", "75013", "Paris");
		
		AddressService addService = context.getBean(AddressService.class);
		addService.createOrUpdateAddress(add1);
		addService.createOrUpdateAddress(add2);
		addService.createOrUpdateAddress(add3);
		addService.createOrUpdateAddress(add4);
		
		Notes not1 = new Notes(2, 2, 2, 2, 2003);
		Notes not2 = new Notes(5, 5, 5, 5, 2005);
		
		NotesService notService = context.getBean(NotesService.class);
		notService.createOrUpdateNotes(not1);
		notService.createOrUpdateNotes(not2);
		
		/*List<Address> addList1 = new ArrayList();
		addList1.add(add1);
		addList1.add(add2);

		List<Address> addList2 = new ArrayList();
		addList2.add(add3);
		addList2.add(add4);

		emp1.setAddressList(addList1);
		emp2.setAddressList(addList2);*/

		//List<Address> addList =  addService.findByEmployee_Id(1);
		

		

	}




}


/*EmployeeService empService = context.getBean(EmployeeService.class);

    	List<Employee> empList = empService.findAll();

    	Employee empLP = empService.findByLoginandPassword("titi", "tiriri");

    	for(Employee employee : empList) {
    		System.out.println("Login = " + employee.getLogin() + " Password = " + employee.getPassword());
    	}

    	System.out.println("User nom : " + empLP.getNom());*/





