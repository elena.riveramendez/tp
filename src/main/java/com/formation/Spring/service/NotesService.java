package com.formation.Spring.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.formation.Spring.models.Notes;
import com.formation.Spring.repo.NotesRepository;

@Service
public class NotesService {

	@Autowired
	private NotesRepository notesRepository;
	
	public void createOrUpdateNotes(Notes not) {
		this.notesRepository.save(not);
	}
		
	public void deteleUser(Notes not) {
		this.notesRepository.delete(not);
	}
	
	public List<Notes> findAll(){
		return this.notesRepository.findAll();	
	}
	
	
	

}
