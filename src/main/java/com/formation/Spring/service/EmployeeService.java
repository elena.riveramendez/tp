package com.formation.Spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.formation.Spring.models.Employee;
import com.formation.Spring.repo.EmployeeRepository;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	public void createOrUpdateEmployee(Employee emp) {
		this.employeeRepository.save(emp);
	}
		
	public void deteleUser(Employee emp) {
		this.employeeRepository.delete(emp);
	}
	
	public List<Employee> findAll(){
		return this.employeeRepository.findAll();	
	}
	
	public Employee findByLoginandPassword(String login, String password) {
		return this.employeeRepository.findByLoginAndPassword(login, password);
	}
	
	public Page<Employee> findAll(Pageable pageable){
		return this.employeeRepository.findAll(pageable);
	}
}
