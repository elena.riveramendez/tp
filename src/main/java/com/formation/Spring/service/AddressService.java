package com.formation.Spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.formation.Spring.models.Address;

import com.formation.Spring.repo.AddressRepository;


@Service
public class AddressService {

	@Autowired
	private AddressRepository addressRepository;
	
	public void createOrUpdateAddress(Address add) {
		this.addressRepository.save(add);
	}
		
	public void deteleAddress(Address add) {
		this.addressRepository.delete(add);
	}
	
	public List<Address> findAll(){
		return this.addressRepository.findAll();	
	}
	
	
	public Page<Address> findAll(Pageable pageable){
		return this.addressRepository.findAll(pageable);
	}
}
