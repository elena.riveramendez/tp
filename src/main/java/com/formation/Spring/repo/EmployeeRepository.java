package com.formation.Spring.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.formation.Spring.models.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer>{

	public List<Employee> findByNom(String n);
	public Page<Employee> findByNom(String n, Pageable pageable);
	public List<Employee> findDistinctByRole(String role);
	
	/*select distinct role
	 * from Employees
	 */
	
	public Employee findByLoginAndPassword(String l, String p);
	
	/* select*
	 * from Employee
	 * where login like "l"
	 * and password like "p"
	 */
	
	
	public Page<Employee> findAll(Pageable pageable);
}
