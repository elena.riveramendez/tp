package com.formation.Spring.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.formation.Spring.models.Notes;

public interface NotesRepository extends JpaRepository<Notes, Integer>{

	public List<Notes> findById(int n);
	
	
	/*select distinct role
	 * from Employees
	 */
	

	
	
	public Page<Notes> findAll(Pageable pageable);
}
