package com.formation.Spring.repo;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.formation.Spring.models.Address;

public interface AddressRepository extends JpaRepository<Address, Integer>{

	public List<Address> findByStreet(String s);
	public Page<Address> findByStreet(String s, Pageable pageable);
	public List<Address> findDistinctByStreet(String street);
	
	/*select distinct role
	 * from Employees
	 */

	public Page<Address> findAll(Pageable pageable);
	
	public List<Address> findByEmployee_Id(Integer id); 
}
